/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework;
import java.util.Scanner;
/**
 *
 * @author MSI-PC
 */
public class Equilateral_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("length :");
        double sidelength = scanner.nextDouble();
        
        double cells_area = (Math.sqrt(3) / 4) * sidelength * sidelength;
        System.out.println("area of​​ equilateral triangle​ is : " + cells_area);
    }
}
