/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_1 {

    public static void main(String[] args) {
        user_input();
    }

    public static void user_input() {

        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("=========== កម្មវិធីដើម្បីគណនារកប្រាក់ខែសរុបរបស់បុគ្គលិក ===========");
        System.out.print("Enter your total working hours :");
        while (!scanner.hasNextInt()) {
            System.err.print("is invalid");
            scanner.next();
        }
        int hour = scanner.nextInt();
        cal(hour);
        char choice;
        System.out.print("Would you like to continue[y/n] :");
        choice = scanner.next().charAt(0);
        if (choice == 'Y' || choice == 'y') {
            user_input();
        } else if (choice == 'N' || choice == 'n') {
            System.out.print("Good bye !");
        }
        scanner.close();
    }

    public static void cal(int hours) {
        int result, overTime, basic_salary, total_salary, de;
        basic_salary = 100;
        if (hours == 50) {
            System.out.print("Basic Salary :" + basic_salary + "$");
            System.out.println();
            System.out.print("This month your salary :" + basic_salary + "$");
            System.out.println();
        } else if (hours < 50) {
            result = 50 - hours;
            de = result * 4;
            total_salary = basic_salary - de;
            System.out.print("Basic Salary :" + basic_salary + "$");
            System.out.println();
            System.out.print("Deduct Salary :" + de + "$");
            System.out.println();
            System.out.print("Net salary :" + total_salary + "$");
            System.out.println();
        } else if (hours > 50) {
            result = hours - 50;
            overTime = result * 3;
            total_salary = overTime + basic_salary;
            System.out.print("Basic Salary :" + basic_salary + "$");
            System.out.println();
            System.out.print("Over Time :" + overTime + "$");
            System.out.println();
            System.out.print("Net salary :" + total_salary + "$");
            System.out.println();
        }
    }
}
