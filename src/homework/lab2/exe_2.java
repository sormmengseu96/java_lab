/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_2 {

    public static void main(String[] args) {
        input_time();
    }

    public static void input_time() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("========== Car ticket calculator ==========");
        System.out.print("Enter start hours :");
        while (!scanner.hasNextInt()) {
            System.out.print("Is invalid");
            scanner.next();
        }
        int start_h = scanner.nextInt();
        System.out.print("Enter end hours :");
        while (!scanner.hasNextInt()) {
            System.out.print("Is invalid");
            scanner.next();
        }
        int end_h = scanner.nextInt();
        cal(start_h, end_h);
        char choice;
        System.out.print("Would you like to continue[y/n]:");
        choice = scanner.next().charAt(0);
        if (choice == 'Y' || choice == 'y') {
            input_time();
        } else if (choice == 'N' || choice == 'n') {
            System.out.print("Good bye !");
        }
        scanner.close();
    }

    public static void cal(int start_h, int end_h) {
        int total_h = 0, pay, section_1, section_2;
        if (start_h >= 1 && end_h <= 12) {
            total_h = end_h - start_h;
            pay = total_h * 1000;
            System.out.print("Total time is :" + total_h + " hours");
            System.out.println();
            System.out.print("Total pay is :" + pay + " R");
            System.out.println();
        } else if (start_h >= 12 && end_h <= 24) {
            total_h = end_h - start_h;
            pay = total_h * 2000;
            System.out.print("Total time is :" + total_h + " hours");
            System.out.println();
            System.out.print("Total pay is :" + pay + " R");
            System.out.println();
        } else if (start_h <= 12 && end_h >= 12 && end_h <= 24) {

            section_1 = 12 - start_h;
            section_2 = end_h - 12;
            total_h = section_1 + section_2;
            pay = (section_1 * 1000) + (section_2 * 2000);
            System.out.print("Total hours :" + total_h + " hours");
            System.out.println();
            System.out.print("section 1 : " + section_1 + " hours");
            System.out.println();
            System.out.print("section 2 : " + section_2 + " hours");
            System.out.println();
            System.out.print("Total pay : " + pay + " R");
            System.out.println();

        }

    }
}
