/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_3 {

    public static void main(String[] args) {
        input();
    }

    public static void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("========= កម្មវិធីដើម្បីដោះស្រាយសមីការដឺក្រទី ២ (aX2 + bX + C = 0) =========");
        System.out.print("a :");
        while (!scanner.hasNextFloat()) {
            System.out.println("is invalid");
            scanner.next();
        }
        float a = scanner.nextFloat();
        System.out.print("c :");
        while (!scanner.hasNextFloat()) {
            System.out.println("is invalid");
            scanner.next();
        }
        float c = scanner.nextFloat();
        System.out.print("b :");
        while (!scanner.hasNextFloat()) {
            System.out.println("is invalid");
            scanner.next();
        }
        float b = scanner.nextFloat();
        find_beta(b, a, c);
        char choice;
        System.out.print("Would you like to continue [y/n] :");
        choice = scanner.next().charAt(0);
        if (choice == 'Y' || choice == 'y') {
            input();
        } else if (choice == 'N' || choice == 'n') {
            System.out.print("Good bye !");
        }
        scanner.close();
    }

    public static void find_beta(float b, float a, float c) {
        float beta, x;
        double x1, x2;
        beta = b * b - 4 * a * c;
        if (beta < 0) {
            System.out.print("No root");
        } else if (beta == 0) {
            x = -b / (2 * a);
            System.out.print("x :" + x);
        } else if (beta > 0) {
            x1 = (-b - Math.sqrt(beta)) / (2 * a);
            x2 = (-b + Math.sqrt(beta)) / (2 * a);
            System.out.print("x1 :" + x1);
            System.out.println();
            System.out.print("x2 :" + x2);
        }
        System.out.println();
    }

}
