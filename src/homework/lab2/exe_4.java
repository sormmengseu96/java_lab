/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_4 {

    public static void main(String[] args) {
        user_input();
    }

    public static void user_input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("========= គណនា និទ្ទេសរបស់សិស្សត =========");
        System.out.print("- Please input the score :");
        int score;
        if (scanner.hasNext()) {
            score = scanner.nextInt();
            cal(score);
            char choice;
            System.out.print("Would you like to continue[y/n] ?:");
            choice = scanner.next().charAt(0);
            if (choice == 'Y' || choice == 'y') {
                user_input();
            } else if (choice == 'N' || choice == 'n') {
                System.out.print("Good bye !");
            }
        } else {
            System.out.print("Is invalid");
        }
        scanner.close();
    }

    public static void cal(int score) {
        if (score <= 100 && score >= 91) {
            System.out.print("- Your grade is : A");
        } else if (score >= 81 && score <= 90) {
            System.out.print("- Your grade is : B");
        } else if (score >= 71 && score <= 80) {
            System.out.print("- Your grade is : C");
        } else if (score >= 61 && score <= 70) {
            System.out.print("- Your grade is : D");
        } else if (score >= 50 && score <= 60) {
            System.out.print("- Your grade is : E");
        } else if (score < 50) {
            System.out.print("- Your grade is : F");
        }
        System.out.println();
    }
}
