/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_4_1 {

    public static void main(String[] args) {
        input_();
    }

    static void input_() {
        char choice;
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("======== calculate the selling price for customer of the Godden Rice Shop ========");
        System.out.print("The weight of rice :");
        int weight_of_rice;
        if (scanner.hasNextInt()) {
            weight_of_rice = scanner.nextInt();
            cal(weight_of_rice);
            System.out.print("Would you like to continue[y/n] ? :");
            choice = scanner.next().charAt(0);
            if (choice == 'Y' || choice == 'y') {
                input_();
            } else if (choice == 'n' || choice == 'N') {
                System.out.print("Good Bye");
            }
        } else {
            System.out.print("is invalid");
        }
        scanner.close();
    }

    static void cal(int weight_of_rice) {

        int result = 0;
        if (weight_of_rice < 100) {
            result = weight_of_rice * 1200;
        } else if (weight_of_rice <= 500 && weight_of_rice >= 100) {
            result = (1200 * weight_of_rice) - (1200 * weight_of_rice) * 15 / weight_of_rice;
        } else if (weight_of_rice > 500) {
            result = (1200 * weight_of_rice) - (1200 * weight_of_rice) * 25 / weight_of_rice;
        }
        System.out.print("Selling pirce :" + result + " R");
        System.out.println();
    }
}
