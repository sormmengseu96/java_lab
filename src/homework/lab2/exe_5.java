/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab2;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class exe_5 {

    public static void main(String[] args) {
        user_input();
    }

    public static void user_input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("=========== គណនារកពន្ឋលើប្រាក់ខែរបស់បុគ្គលិក ==========");

        System.out.print("Input the salary :");
        int salary_;
        if (scanner.hasNextInt()) {
            salary_ = scanner.nextInt();
            cal(salary_);
            char choice;
            System.out.print("Would you like to continue[y/n] ? :");
            choice = scanner.next().charAt(0);
            if (choice == 'Y' || choice == 'y') {
                user_input();
            } else if (choice == 'N' || choice == 'n') {
                System.out.print("Good bye !");
            }
        } else {
            System.out.print("Is invalid");
        }
        scanner.close();
    }

    public static void cal(int salary) {
        double tax;
        if (salary <= 500000) {
            System.out.print("Tax :" + 0 + "%");
            System.out.println();
            System.out.print("Rate is :" + 0 + "%");
        } else if (salary >= 500001 && salary <= 1250000) {
            tax = (salary * 0.5) - 25000;
            System.out.print("Tax :" + 5 + "%");
            System.out.println();
            System.out.print("Rate is :" + tax);
        } else if (salary >= 1250001 && salary <= 8500000) {
            tax = (salary * 0.10) - 87500;
            System.out.print("Tax :" + 10 + "%");
            System.out.println();
            System.out.print("Rate is :" + tax);
        } else if (salary >= 8500001 && salary <= 12500000) {
            tax = (salary * 0.15) - 512500;
            System.out.print("Tax :" + 15 + "%");
            System.out.println();
            System.out.print("Rate is :" + tax);
        } else if (salary >= 12500001) {
            tax = (salary * 0.20) - 1137000;
            System.out.print("Tax :" + 20 + "%");
            System.out.println();
            System.out.print("Rate is :" + tax);
        }
        System.out.println();

    }
}
