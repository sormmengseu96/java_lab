/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package homework.lab3;

import java.util.Scanner;

/**
 *
 * @author MSI-PC
 */
public class array_2 {

    public static void main(String[] args) {
        System.err.println("-------------------------------------------------------------------------");
        input();
    }

    public static void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input number of product:");
        if (scanner.hasNextInt()) {
            try {
                int n = scanner.nextInt();
                Object[][] sale = new Object[n][4];
                for (int i = 0; i <= n - 1; i++) {
                    System.out.print("Product name: ");
                    scanner = new Scanner(System.in);
                    sale[i][0] = scanner.nextLine();
                    System.out.print("Price :");
                    scanner = new Scanner(System.in);
                    sale[i][1] = scanner.nextInt();
                    System.out.print("Qty :");
                    scanner = new Scanner(System.in);
                    sale[i][2] = scanner.nextInt(); 
                    System.out.print("Discount(%) :");
                    scanner = new Scanner(System.in);
                    sale[i][3] = scanner.nextInt();
                }
                System.err.println("-------------------------------------------------------------------------");
                System.out.println("[+]\tproudct name\tprice\tqty\tdis(%)\tdis_amt\tamount");
                int amount = 0;
                int total = 0;
                float net_dis = 0;
                for (int i = 0; i < n; i++) {
                    int price = (int) sale[i][1];
                    int qty = (int) sale[i][2];
                    float dis = (int) sale[i][3];
                    double dis_amt = (price * qty) * (dis / 100);
                    amount = price - (int) dis_amt;
                    total += amount;
                    net_dis += dis;
                    System.out.println((i + 1) + "\t" + sale[i][0] + "\t\t" + sale[i][1] + "\t" + sale[i][2] + "\t" + sale[i][3] + "\t" + (int) dis_amt + "\t" + amount);
                }
                System.err.println("-------------------------------------------------------------------------");
                System.out.println("Net Discount :" + (int) net_dis);
                System.out.println("Net Total :" + total);
                cont();
            } catch (Exception e) {
                System.err.print("error =>" + e);
            }
        } else {
            System.err.println("is invalid !");
            cont();
        }
    }

    public static void cont() {
        Scanner scanner = new Scanner(System.in);
        char choice;
        System.out.print("Would you like to continue[y/n]? :");
        choice = scanner.next().charAt(0);
        if (choice == 'Y' || choice == 'y') {
            input();
        } else {
            System.err.print("\nGood bye !");
        }
    }
}
